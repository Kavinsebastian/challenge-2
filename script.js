let batu = document.getElementById('batu');
let kertas = document.getElementById('kertas');
let gunting = document.getElementById('gunting');
let activeClass = document.getElementsByClassName('active')
let result = document.getElementById('versus')
let icon = ['batu','kertas','gunting']
let refresh = document.getElementById('reset')
let start = 'VS' ;
let pScore = document.getElementById('pScore')
let cScore = document.getElementById('cScore')
let tombol = document.getElementById('tombol')


tombol.addEventListener('click',function(e){
    if(cScore.textContent == 0 && pScore.textContent == 0){
         begin()
    }
})

function begin (){
        
    result.textContent = 'Go!'
    
    function freset() {
        result.textContent = start
        kertas.classList.remove('active')
        gunting.classList.remove('active')
        batu.classList.remove('active')
    
        icon.forEach((item)=> {
            let compClearElement = document.getElementById(`${item}-com`)
            compClearElement.classList.remove('active')
    
            result.style.fontSize = '10vw'
            result.style.color = 'red'
        })
    }

    refresh.addEventListener('click', event =>{
        if(result.textContent === skor[0]){
            freset()
        }else if(result.textContent === skor[1]){
            freset()
        }else if(result.textContent === skor[2]){
            freset()
        }
        
    })
    
    batu.addEventListener('click', event =>{
        if (activeClass.length < 2){
           kertas.classList.remove('active')
           gunting.classList.remove('active')
           batu.classList.add('active')
           suit('batu')
       }
   })
   kertas.addEventListener('click', event =>{
       if (activeClass.length < 2){
       batu.classList.remove('active')
       gunting.classList.remove('active')
       kertas.classList.add('active')
       suit('kertas')
        }
   })
   gunting.addEventListener('click', event =>{
       if (activeClass.length < 2){
       batu.classList.remove('active')
       kertas.classList.remove('active')
       gunting.classList.add('active')
       suit('gunting')
        }
  


})
   
}
function suit (choice) {
    
    
    let player = choice
    let logic = Math.floor(Math.random() * 3 )
    let comp = icon[logic]
    let hasil = rules(player,comp)

    
    icon.forEach((item)=> {
        let compClearElement = document.getElementById(`${item}-com`)
        compClearElement.classList.remove('active')
    })
    let  compElement= document.getElementById(`${comp}-com`)
    compElement.classList.add('active')
   
    result.textContent = hasil
    

    if (hasil === 'YOU WIN') {
        let textScore = parseInt(pScore.textContent)
        pScore.textContent = textScore + 1   
        result.style.fontSize = '5vw'
        result.style.color = 'blue'
    }
    if (hasil === 'DRAW'){
        result.style.fontSize = '5vw'
        result.style.color = 'silver'
    }
    if (hasil === 'COM WIN'){
        let cTextScore = parseInt(cScore.textContent)
        cScore.textContent = cTextScore + 1
        result.style.fontSize = '5vw'
        result.style.color = 'green'
    }
}

let skor = ['DRAW','YOU WIN','COM WIN']
    function rules (player,comp){
        if (player === comp) return skor[0]
        
        if ( player === 'batu' && comp === 'gunting'){ 
            return skor[1]
        } if ( player === 'kertas' && comp === 'batu'){
            return skor[1]
        } if ( player === 'gunting' && comp === 'kertas'){
            return skor[1]
        } 
        if ( comp === 'batu' && player === 'gunting'){ 
            return skor[2]
        } if ( comp === 'kertas' && player === 'batu'){
            return skor[2]
        } if ( comp === 'gunting' && player === 'kertas'){
            return skor[2]
        }
}



